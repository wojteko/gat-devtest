# Dev Test

## Context
* This test is not about solving a problem. It's about execution and checking what's your default way of thinking and coding habits.
* Introducing new gems to the project is permitted.

## Fork this repository
* All your changes should be made in a **private fork** of this repository. When you're done, please:
* **Share your fork with the @wojteko user** (Settings -> Members -> Share with Member)
* Make sure that you grant the user the **Reporter role** so that our reviewers can check out the code using Git.

## Excercise
Prepare a rails app (use the skeleton app to kick start the task), which would have two isolated APIs, public and private:

Private API responding to the following requests:

  * 1 - `GET  target_groups/:country_code`
  * 2 - `POST evaluate_target`

Public API responding to the following requests

  * 3 - `GET  target_groups/:country_code`
  
Access to private endpoints should be possible only via a valid access token (no need for users in the app). 
You should assume there is no firewall so the server would be public-facing and needs to be secured properly


### Models:

#### Provided

PanelProvider:
  * `id`, `code`

Country:
  * `id`, `code`, `panel_provider_id`

Location:
  * `id`, `name`, `external_id`, `secret_code`

#### Please add

LocationGroup:
  * `id`, `name`, `country_id`, `panel_provider_id`

`Location` is linked to `LocationGroup` via many-to-many relationship.

TargetGroup model should have associations with itself via parent_id which would form a tree with multiple root nodes:
  * `id`, `name`, `external_id`, `parent_id`, `secret_code`, `panel_provider_id`

Country is linked with LocationGroup via one to many relationship and with TargetGroup via many to many
but only with the root nodes.

### Data

Can be initialized by seeding the app.

#### Provided

  - 3 Panel Providers
  - 3 Countries, each with different panel provider
  - 20 Locations

#### Please add

  - 4 Location Groups, 3 of them with different provider and 1 would belong to any of them
  - 4 root Target Groups and each root should start a tree which is minimum 3 levels deep (3 of them with different provider and 1 would belong to any of them)


## Panel providers pricing logic

Each panel provider will have a different pricing logic

#### Panel 1

The price should be based on how many letters "a" can you find in the text nodes on this site http://time.com divided by 100

#### Panel 2

The price should be based on how many arrays with more than 10 elements you can find in this search result

http://openlibrary.org/search.json?q=the+lord+of+the+rings

#### Panel 3

The price should be based on how many html nodes can you find on this site http://time.com divided by 100


## Request info

#### Request #1

It should return target groups which belong to the selected country based on it's current panel provider

#### Request #2

It should require all of the following params to be provided and valid:

- `:country_code`
- `:locations`  (an array of hashes which look like this `{ id: 123, panel_size: 200 }`) 
  - array must contain at least one location
  - `{id, panel_size}` format of location hash must be used
  - all locations must exist (by `id`)
  - `panel_size` must be an integer

and returns a price that is based on a logic specific to a panel provider assigned to a given country and panel sizes (`sum_of_panel_sizes * price_from_panel provider_pricing`). 

#### Request #3

Same as #1 but for public consumption
